/**
 * Created by Elisabeth on 14.06.2015.
 */
/** Converts numeric degrees to radians */
if (typeof(Number.prototype.myRad) === "undefined") {
    Number.prototype.myRad = function() {
        return this * Math.PI / 180;
    }
}


function mything (){
    var lat1 = parseFloat(document.getElementById("lat1").value);
    var lat2 = parseFloat(document.getElementById("lat2").value);
    var lon1 = parseFloat(document.getElementById("lon1").value);
    var lon2 = parseFloat(document.getElementById("lon2").value);

    var R = 6371000; // metre
    var phi1 = lat1.myRad();
    var phi2 = lat2.myRad();
    var deltaPhi = (lat2-lat1).myRad();
    var deltaLamda = (lon2-lon1).myRad();

    var a = (Math.sin(deltaPhi/2) * Math.sin(deltaPhi/2) +
    Math.cos(phi1) * Math.cos(phi2) *
    Math.sin(deltaLamda/2) * Math.sin(deltaLamda/2));
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;

    document.getElementById("distance").value = d;
}