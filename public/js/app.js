(function(){
    var App = angular.module('eventCollection', ['ui.router', 'js-data', 'ngMap', 'ngFileUpload']);

    App.factory('Event', ['DS',function(DS){
        return DS.defineResource('event');
    }]);

    App.factory('User', ['DS',function(DS){
        return DS.defineResource('user');
    }]);

    App.factory('Message', ['DS',function(DS){
        return DS.defineResource('message');
    }]);

    App.factory('HostEvent', ['DS',function(DS){
        return DS.defineResource('hostEvent');
    }]);

    App.factory('GuestEvent', ['DS',function(DS){
        return DS.defineResource('guestEvent');
    }]);

    App.factory('Registration', ['DS',function(DS){
        return DS.defineResource({
            name:'registration',
            idAttribute: 'id'
        });
    }]);

    App.factory('authHttpResponseInterceptor',['$q','$location',function($q,$location){
        return {
            response: function(response){
                if (response.status === 401) {
                    console.log("Response 401");
                }
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                if (rejection.status === 401) {
                    debugger;
                    console.log("Response Error 401",rejection);
                    $location.path('/login').search('returnTo', $location.path());
                }
                return $q.reject(rejection);
            }
        };
    }]);

    App.run(["$rootScope", "$state", function($rootScope, $state){
       $rootScope.$state = $state;
        $rootScope.$on('$stateChangeSuccess', function(){
            window.scrollTo(0, 0);
        })
    }]);

    App.config(['$stateProvider', 'DSProvider', '$urlRouterProvider','$httpProvider', function($stateProvider, DSProvider, $urlRouterProvider, $httpProvider){

        $urlRouterProvider.when('','/');
        $urlRouterProvider.when('api/my_hosted_events', '/my_hosted_events');
        DSProvider.defaults.basePath = '/api';

        //Http Interceptor to check auth failures for xhr requests
        $httpProvider.interceptors.push('authHttpResponseInterceptor');

        /*
        * information on home screen template, no interaction in js
        * */
        var home_config = {
            url: '/',
            templateUrl: 'assets/templates/home.html'
        };


        /*
         * interaction for the template search page "all_events_by_date"
         * default search of all future events from db by date
         * includes manage user interaction to register for an event
         * */
        var show_all_by_date_config = {
            url: '/show_all_by_date',
            templateUrl: 'assets/templates/all_events_by_date.html',
            resolve: {
                events: ['Event', function(Event){ return Event.findAll({}, {bypassCache: true});}]
            },
            controller: ['$scope', 'events', '$state', 'Registration', function($scope, events, $state, Registration){
                $scope.events = events;
                //ads randomize if no picture is uploaded
                $scope.other = Math.floor((Math.random() * 6) + 1);
                $scope.eventregistration = Registration.createInstance();
                $scope.success = true;

                //save a registration to an event
                $scope.save = function() {
                    $scope.eventregistration.DSCreate().then(function(){
                        $scope.eventregistration = Registration.createInstance();
                        $scope.success = true;
                    })
                };

                //invoke register saving function for this event
                $scope.registerNow = function(event) {
                    $scope.eventregistration.event = event;
                    $scope.save()
                };

                //manage success message in case user sent registration
                $scope.noSuccess = function() {
                    $scope.success = false;
                };
            }]
        };

        /*
        * interaction for the template search page "all_events_by_distance"
        * default search of all future events from db by closest distance to an entered location via googlemaps autocomplete
        * includes manage user interaction to register for an event
        * */
        var show_all_by_distance_config = {
            url: '/show_all_by_distance',
            templateUrl: 'assets/templates/all_events_by_distance.html',
            resolve: {
                events: ['Event', function(Event){ return Event.findAll({}, {bypassCache: true});}]
            },
            controller: ['$scope', 'events', '$state', 'Registration', '$window', '$timeout', function($scope, events, $state, Registration, $window, $timeout){

                //distance calculation of two locations on earth via latitude and longitude
                $scope.haversineformula = function (lat1, lng1, latEvent, lngEvent) {
                    var R = 6371000; // metre
                    var phi1 = (lat1 * Math.PI / 180);
                    var phi2 = (latEvent * Math.PI / 180);
                    var deltaPhi = ((latEvent - lat1) * Math.PI / 180);
                    var deltaLamda = ((lngEvent - lng1) * Math.PI / 180);
                    var a =
                        Math.sin(deltaPhi / 2) * Math.sin(deltaPhi / 2) +
                        Math.cos(phi1) * Math.cos(phi2) *
                        Math.sin(deltaLamda / 2) * Math.sin(deltaLamda / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    return Math.round(R * c);
                };

                //receive input location from html, converts it into geolocations (lat,long), and
                //invoke distance calculation for all events to this address
                $scope.calcfunct =  function () {
                    var lat1 = 0.0;
                    var lng1 = 0.0;
                    var distance = 0.0;
                    var addressUser = document.getElementById("address").value;
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({"address": addressUser}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            lat1 = location.lat();
                            lng1 = location.lng();
                        }
                    });

                    $timeout( function () {
                            for (var i = 0; i < events.length; i++) {
                                var e = events[i];
                                var latEvent = e.latitude;
                                var lngEvent = e.longitude;
                                distance = $scope.haversineformula(lat1, lng1, latEvent, lngEvent);
                                e.distance = distance;
                            }
                            $scope.events = events;
                        }, 1000
                    )
                };

                $scope.showByDistance = function (){
                    $scope.calcfunct();
                };

                $scope.events = events;
                //ads randomize if no picture is uploaded
                $scope.other = Math.floor((Math.random() * 6) + 1);
                $scope.eventregistration = Registration.createInstance();
                $scope.success = false;

                //save a registration to an event
                $scope.save = function() {
                    $scope.eventregistration.DSCreate().then(function(){
                        $scope.eventregistration = Registration.createInstance();
                        $scope.success = true;
                    })
                };

                //invoke register saving function for this event
                $scope.registerNow = function(event) {
                    $scope.eventregistration.event = event;
                    $scope.save()
                };

                //manage success message in case user sent registration
                $scope.noSuccess = function() {
                    $scope.success = false;
                };
            }]
        };


        /*
        * interaction for adding a new event to the db including data+files input
        * */
        var add_new_config = {
            url: '/add_event',
            templateUrl: 'assets/templates/add_new.html',
            controller: ['$scope', 'Event', 'Upload', function($scope, Event, Upload){
                $scope.event = Event.createInstance();
                $scope.success = false;
                $scope.editAble = false;

                //picture upload to server folder
                $scope.files = [];
                //adapted from https://github.com/danialfarid/ng-file-upload
                $scope.upload = function (file) {
                    Upload.upload({
                        url: 'public/images/users',
                        fields: {"picName": "picName"},
                        file: file
                    });
                };

                //picture preview
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        $scope.upload(input.files[0]);
                        reader.onload = function (e) {
                            $('#preview').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                //on change picture preview function is loaded
                $("#picture").change(function(){
                    readURL(this);
                });

                //actually save the event data into the db
                $scope.save = function() {
                    $scope.event.DSCreate().then(function(){
                        $scope.event = Event.createInstance();
                        $scope.success = true;
                    })
                };
            }]
        };


        /*
        * interaction for according html template my_hosted_events
        * retrieves all existing events for a user as host
        * includes manage registrations and messages for an event
        * includes remove and edit an event
        * */
        var my_hosted_events_config = {
            url: '/my_hosted_events',
            templateUrl: 'assets/templates/my_hosted_events.html',
            resolve: {
                hostEvents: ['HostEvent', function(HostEvent){ return HostEvent.findAll({}, {bypassCache: true});}]
            },
            controller: ['$scope', 'hostEvents', '$state', function($scope, hostEvents, $state){
                $scope.hostEvents = hostEvents;
                //the difference parameter to the add_new version for editing an event
                $scope.editAble = false;
                $scope.success = $scope.hostEvents[0] != undefined;

                //manage existing registrations for an event
                $scope.showRegistrations = function(event) {
                    $state.go('my_hosted_events.registrations', {eventId : event.id});
                    //location.reload(true);
                };

                //manage messages for an event
                $scope.messageParticipants = function(event) {
                    $state.go('my_hosted_events.new_message', {eventId : event.id});
                    //location.reload(true);
                };

                //manage delete event from db (->in java: according registrations will be deleted, too)
                $scope.removeEvent = function(event) {
                    $scope.event = event;
                    $scope.event.DSDestroy();
                    location.reload(true);
                };

                //manage edit event interaction
                $scope.editEvent = function(event) {
                    $scope.event = event;
                    $scope.editAble = true;
                    $state.go('my_hosted_events.edit_event', {event : event});
                };
            }]
        };

        /*
        * interaction for editing an existing event of the db including data+files input
        * uses same html template as for adding new events "add_new_html", but with parameter considering from my_hosted_events_config
        * */
        var edit_event_config = {
            url: '/edit_event',
            templateUrl: 'assets/templates/add_new.html',
            controller: ['$scope', 'Event', 'Upload', '$state', function($scope, Event, Upload, $state){
                $scope.success = false;

                //TOFIX file upload .... show date in html

                //picture upload to server folder
                $scope.files = [];
                //adapted from https://github.com/danialfarid/ng-file-upload
                $scope.upload = function (file) {
                    Upload.upload({
                        url: 'public/images/users',
                        fields: {"picName": "picName"},
                        file: file
                    });
                };

                //picture preview
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        $scope.upload(input.files[0]);
                        reader.onload = function (e) {
                            $('#preview').attr('src', e.target.result);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                //on change picture preview function is loaded
                $("#picture").change(function(){
                    readURL(this);
                });

                //actually update the event data in db
                $scope.update = function(event) {
                    $scope.event = event;
                    $scope.event.DSSave();
                    $scope.editAble = false;
                    $scope.success = true;
                    //$state.go ('my_hosted_events');
                };
            }]
        };

        /*
        * interaction for user(host) to manage registrations/applications for his hosted events
        * includes accepting/rejecting applicants
        * */
        var show_registrations_config = {
            url: '/registrations/{eventId}',
            templateUrl: 'assets/templates/show_registrations.html',
            resolve: {
                registrations: ['Registration', '$stateParams', function(Registration, $stateParams){
                    return Registration.find($stateParams.eventId);
                }]
            },
            controller: ['$scope', 'registrations', function($scope, registrations){
                $scope.registrations = registrations;
                $scope.success = false;
                $('#RegistrationModal').modal('show');

                //accept a registration by a user(guest) and update booleans in db -> later used for feedback for guest+host
                $scope.acceptNow = function(registration) {
                    if (!$scope.success) {
                        $scope.registration = registration;
                        $scope.registration.rejected = false;
                        $scope.registration.accepted = true;
                        $scope.registration.DSSave();
                    }
                };

                //reject a registration by a user(guest) and update booleans in db -> later used for feedback for guest+host
                $scope.rejectNow = function(registration) {
                    $scope.registration = registration;
                    $scope.registration.accepted = false;
                    $scope.registration.rejected = true;
                    $scope.registration.DSSave();
                    $scope.success = true;
                };

                $scope.done = function() {
                    //$state.go('my_hosted_events', {eventId : event.id})
                };
            }]
        };

        /*
        * interaction for user(host) to manage broadcast-messages to all participants (host+accepted guests) for his hosted events
        * includes retrieving sent messages for an event and option to send new messages
        * */
        var new_host_message_config = {
            url: '/new_message/{eventId}',
            templateUrl: 'assets/templates/new_message.html',
            resolve: {
                messages: ['Message', '$stateParams', function(Message, $stateParams){
                    return Message.find($stateParams.eventId);
                }]
            },
            controller: ['$scope', 'messages', '$stateParams', 'Message', 'Event', function($scope, messages, $stateParams, message, event){
                $scope.message = message.createInstance();
                $scope.messages = messages;
                $scope.send = false;
                $scope.event = event.createInstance();
                //link message to an event
                $scope.event.id = $stateParams.eventId;
                $scope.message.event = $scope.event;
                $('#MessageModal').modal('show');

                //save a new message to the db
                $scope.sendMessage = function() {
                    $scope.message.text = "[HOST] " + $scope.message.text;
                    $scope.message.DSCreate().then(function(){
                        $scope.message = message.createInstance();
                        $scope.send = true;
                    });
                    location.reload(true);
                };
            }]
        };


        /*
        * interaction for according html template my_guest_events
        * retrieves all existing registrations for a user -> according event information is shown in html
        * includes manage registrations and messages for an event
        * includes remove an existing registration
        * */
        var my_guest_events_config = {
            url: '/my_guest_events',
            templateUrl: 'assets/templates/my_guest_events.html',
            resolve: {
                guestRegistrations: ['Registration', function(Registration){
                    return Registration.findAll({bypassCache: true});
                }]
            },
            controller: ['$scope', 'guestRegistrations', '$state', function($scope, guestRegistrations, $state){
                $scope.guestRegistrations = guestRegistrations;
                $scope.success = $scope.guestRegistrations[0] != undefined;

                //manage deletion of a single user registration from db
                $scope.removeRegistration = function(registration) {
                    $scope.registration = registration;
                    //destroy only registration, not whole event
                    $scope.registration.DSDestroy();
                    location.reload(true);
                };

                //manage messages for an event of a registration (will only be invoked if guest user is accepted to an event)
                $scope.messages = function(registration) {
                    $state.go('my_guest_events.new_message', {eventId : registration.event.id});
                    location.reload(true);
                };
            }]
        };

        /*
         * interaction for user(guest) to manage broadcast-messages to all participants (host+accepted guests) for an events
         * includes retrieving sent messages for an event and option to send new messages
         * currently almost the same as new_host_message_config, except reload and GUEST marker, but could be used to specify...
         * ...view/send restrictions later on, e.g. only one message sent possible etc.
         * */
        var new_guest_message_config = {
            url: '/new_message/{eventId}',
            templateUrl: 'assets/templates/new_message.html',
            resolve: {
                messages: ['Message', '$stateParams', function(Message, $stateParams){
                    return Message.find($stateParams.eventId);
                }]
            },
            controller: ['$scope', 'messages', '$stateParams', 'Message', 'Event', function($scope, messages, $stateParams, message, event){
                $scope.message = message.createInstance();
                $scope.messages = messages;
                $scope.send = false;
                $scope.event = event.createInstance();
                //link message to an event
                $scope.event.id = $stateParams.eventId;
                $scope.message.event = $scope.event;
                $('#MessageModal').modal('show');

                //save a new message to the db
                $scope.sendMessage = function() {
                    $scope.message.text = "[GUEST] " + $scope.message.text;
                    $scope.message.DSCreate().then(function(){
                        $scope.message = message.createInstance();
                        $scope.send = true;
                    });
                    location.reload(true);
                };
            }]
        };

        //state providers
        $stateProvider.state('home', home_config);

        $stateProvider.state('show_all_by_date', show_all_by_date_config);
        $stateProvider.state('show_all_by_distance', show_all_by_distance_config);

        $stateProvider.state('add_event', add_new_config);

        $stateProvider.state('my_hosted_events', my_hosted_events_config);
        $stateProvider.state('my_hosted_events.edit_event', edit_event_config);
        $stateProvider.state('my_hosted_events.registrations', show_registrations_config);
        $stateProvider.state('my_hosted_events.new_message', new_host_message_config);

        $stateProvider.state('my_guest_events', my_guest_events_config);
        $stateProvider.state('my_guest_events.new_message', new_guest_message_config);
    }]);

})();
