import play.Project._

name := "hello-play-java"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.2.2",
  "org.webjars.bower" % "bootstrap" % "3.3.4",
  "ws.securesocial" %% "securesocial" % "2.1.4",
  "org.webjars.bower" % "angular" % "1.4.0-rc.2",
  "org.webjars.bower" % "angular-ui-router" % "0.2.14",
  "org.webjars.bower" % "js-data-angular" % "2.4.0",
  "org.webjars.bower" % "ng-file-upload" % "5.0.4",
  "org.webjars" % "jquery" % "2.1.1"
)

libraryDependencies ++= Seq(
  javaJdbc,
  "org.xerial" % "sqlite-jdbc" % "3.8.6",
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  "org.hibernate" % "hibernate-entitymanager" % "4.3.9.Final" // replace by your jpa implementation
)

libraryDependencies += "commons-codec" % "commons-codec" % "1.2"

libraryDependencies += "commons-httpclient" % "commons-httpclient" % "3.1"

libraryDependencies += "com.google.code.geocoder-java" % "geocoder-java" % "0.16"

libraryDependencies += "com.google.code.gson" % "gson" % "2.2.4"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.12"

libraryDependencies += "junit" % "junit" % "4.11"

libraryDependencies += "org.slf4j" % "jcl-over-slf4j" % "1.7.12"

libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.12"

libraryDependencies += "org.apache.poi" % "poi" % "3.12"

libraryDependencies += javaEbean

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

playJavaSettings

