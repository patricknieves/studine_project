package models;

import java.util.Date;
import javax.persistence.*;
import play.db.ebean.Model;

/*
* Model to handle User login status
* adapted for individual use from SecureSocial
* */
@Entity
@Table(name = "LocalToken")
public class LocalToken extends Model {

    private static final long serialVersionUID = 1L;

    @Id
    public String uuid;

    public String email;

    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    public Date createdAt;

    @Column(name = "expireAt")
    @Temporal(TemporalType.TIMESTAMP)
    public Date expireAt;

    @Column(name = "isSignUp")
    public boolean isSignUp;


    public static Finder<String, LocalToken> find = new Finder<String, LocalToken>(
           String.class, LocalToken.class
    );
}

