package models;

import play.db.ebean.Model;
import javax.persistence.*;

/*
* Model to describe a registration for an existing Event by a User
* */
@Entity
@Table(name = "Registration")
public class Registration extends Model {

    @Id
    @Column(name = "id")
    public Integer id;

    public String message;

    public boolean accepted;

    public boolean rejected;

    @ManyToOne
    @JoinColumn(name = "event")
    public Event event;

    @ManyToOne
    @JoinColumn(name = "user")
    public User user;

    @Column(name = "date")
    public String date;

    public static Finder<String, Registration> find = new Finder<String, Registration>(
            String.class, Registration.class
    );
}
