package models;

import play.db.ebean.Model;
import javax.persistence.*;

/*
* Model to describe a broadcast-message sent from a User for an Event visible to all participants
* */
@Entity
@Table(name = "Message")
public class Message extends Model{

    @Id
    public Integer id;

    @ManyToOne
    @JoinColumn(name = "event")
    public Event event;

    @ManyToOne
    @JoinColumn(name = "author")
    public User author;

    @JoinColumn(name = "date")
    public String date;

    @JoinColumn(name = "text")
    public String text;

    public static Finder<String, Message> find = new Finder<String, Message>(
            String.class, Message.class
    );

}
