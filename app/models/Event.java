package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.*;
import java.util.Collection;

/*
* Model to describe an Event published by a User, where other Users can send Registrations for
* */
@Entity
@Table(name = "Event")
public class Event extends Model {

    @Id
    public Integer id;

    @Constraints.Required
    public String title;

    public String description;

    public String location;

    public String motto;

    public String date;

    public Integer capacity;

    @Column(name= "capacityLeft")
    public Integer capacityLeft;

    @Column(name = "isVeggie")
    public Boolean isVeggie;
    @Column(name = "isAlc")
    public Boolean isAlc;
    @Column(name = "isSmoker")
    public Boolean isSmoker;
    @Column(name = "isCouple")
    public Boolean isCouple;

    public Double costs;

    public String picture;

    public Double latitude;

    public Double longitude;

    @ManyToOne
    @JoinColumn(name = "host")
    public User host;

    @OneToMany(mappedBy = "event")
    public Collection<Registration> registrations;

    @OneToMany(mappedBy = "event")
    public Collection<Message> messages;

    public static Finder<String, Event> find = new Finder<String, Event>(
            String.class, Event.class
    );
}
