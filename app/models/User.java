package models;

import javax.persistence.*;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.Id;
import play.db.ebean.Model;

/*
* Model to describe a User on the platform
* */
@Entity
@Table(name = "User")
public class User extends Model {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    public String id;

    public String email;

    public String password;

    @Column(name = "firstName")
    public String firstName;

    @Column(name = "lastName")
    public String lastName;

    public String provider;

    @OneToMany(mappedBy = "user")
    public Collection<Registration> registrations;

    @OneToMany(mappedBy = "host")
    public Collection<Event> hostedEvents;

    @OneToMany(mappedBy = "author")
    public Collection<Message> messages;

    public static Finder<String, User> find = new Finder<String, User>(
            String.class, User.class
    );

    @Override
    public String toString() {
        return this.id + " - " + this.firstName;
    }

}

