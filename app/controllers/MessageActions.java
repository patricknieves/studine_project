package controllers;

import models.Event;
import models.User;
import models.Message;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.db.ebean.Transactional;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import securesocial.core.java.SecureSocial;

/**
 * Controller for Message Model
 * allows Create and Read actions
 */
public class MessageActions extends Controller{

    public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    /*
    * creates a Message by a User for an Event in db
    * */
    @SecureSocial.SecuredAction(ajaxCall = true)
    @Transactional
    public static Result createMessage() {
        if (SecureSocial.currentUser() != null) {
            Message newMessage = Json.fromJson(request().body().asJson(), Message.class);
            String idCurrent = SecureSocial.currentUser().identityId().userId();
            newMessage.author = User.find.byId(idCurrent);
            Date date = new Date();
            newMessage.date = df.format(date);
            newMessage.save();
            return created(Json.toJson(newMessage));
        }
        return notFound();
    }

    /*
    * reads all Messages for an existing Event by all Users from db
    * */
    @SecureSocial.SecuredAction
    @Transactional
    public static Result getMessagesForEvent(int id) {
        if (SecureSocial.currentUser() != null) {
            Event event = Event.find.byId(String.valueOf(id));
            List<Message> allMessages = Message.find.where().eq("event", event).findList();
            return ok(Json.toJson(allMessages));
        }
        return notFound();
    }

}
