package controllers;

import com.avaje.ebean.Ebean;
import models.Event;
import models.Registration;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.db.ebean.Transactional;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import securesocial.core.java.SecureSocial;

/**
 * Controller for Registration Model
 * allows all CRUD actions
 */
public class RegistrationActions extends Controller {

    public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    /*
    * creates a Registration for an Event by a User in db or updates the application message of a Registration if the User already sent a Registration for the Event
    * */
    @SecureSocial.SecuredAction(ajaxCall = true)
    @Transactional
    public static Result createRegistration() {
        Registration newRegistration = Json.fromJson(request().body().asJson(), Registration.class);
        Event event = Event.find.byId(String.valueOf(newRegistration.event.id));

        if (SecureSocial.currentUser() != null) {
            String idCurrent = SecureSocial.currentUser().identityId().userId();
            User currentUser = User.find.byId(idCurrent);
            //find existing registration for this user and this event
            List<Registration> existingRegistration = Registration.find.where().eq("event", event).eq("user", currentUser).findList();

            if (existingRegistration.isEmpty()) {
                newRegistration.user = User.find.byId(idCurrent);

                Date date = new Date();
                newRegistration.date = df.format(date);
                newRegistration.rejected = false;

                newRegistration.save();
                return created(Json.toJson(newRegistration));
            } else {
                // update Registration
                Registration oldRegistration = existingRegistration.get(0);
                oldRegistration.message = oldRegistration.message + "\n" + newRegistration.message;
                oldRegistration.update();
                return ok(Json.toJson(oldRegistration));
            }
        }
        return notFound();
    }

    /*
    * reads all Registrations for all Events by a specific User from db
    * */
    @SecureSocial.SecuredAction
    @Transactional
    public static Result getRegistrationsByUser() {
        String idCurrent = SecureSocial.currentUser().identityId().userId();
        User currentUser = User.find.byId(idCurrent);
        if (currentUser != null) {
            List<Registration> allRegistrations = Registration.find.where().eq("user", currentUser).findList();
            return ok(Json.toJson(allRegistrations));
        }
        return notFound();
    }

    /*
    * reads all Registrations for a specific Event of all Users from db
    * */
    @SecureSocial.SecuredAction
    @Transactional
    public static Result getRegistrationsByEvent(int id) {
        if (SecureSocial.currentUser() != null) {
            Event event = Event.find.byId(String.valueOf(id));
            List<Registration> allRegistrations = Registration.find.where().eq("event", event).findList();
            return ok(Json.toJson(allRegistrations));
        }
        return notFound();
    }

    /*
    * updates an existing Registration in db for a specific Event and a specific User
    * acceptance of Registration leads to decrease of left capacity of the Event
    * */
    @Transactional
    public static Result updateRegistration(int id) {
        if (SecureSocial.currentUser() != null) {
            Registration updatedRegistration = Json.fromJson(request().body().asJson(), Registration.class);
            updatedRegistration.id = id;
            if (updatedRegistration.accepted) {
                Event event = Ebean.find(Event.class, updatedRegistration.event.id);
                event.capacityLeft = (event.capacityLeft - 1);
                event.update();
                updatedRegistration.update();
                return ok(Json.toJson(updatedRegistration));
            } else {
                updatedRegistration.update();
                return ok(Json.toJson(updatedRegistration));
            }
        }
        return notFound();
    }

    /*
        * deletes a specific Registration for a specific Event by a specific User from db
    * */
    @Transactional
    public static Result deleteRegistration(int id) {
        if (SecureSocial.currentUser() != null) {
            Registration registration = Ebean.find(Registration.class, id);
            if (registration.accepted) {
                Event event = Ebean.find(Event.class, registration.event.id);
                event.capacityLeft = (event.capacityLeft + 1);
                event.update();
                Ebean.delete(registration);
                return noContent();
            } else {
                Ebean.delete(registration);
                return noContent();
            }
        }
        return notFound();
    }

}
