package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import models.Event;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.db.ebean.Transactional;

import java.io.File;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import securesocial.core.java.SecureSocial;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.*;

import java.io.IOException;

/*
* Controller for Event Model
* allows all CRUD actions
* */
public class EventActions extends Controller {

    //TOFIX static variable! -> only one upload at a time
    static String tempName = null;
    public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    /*
    * renders main view for a User or unknown User
    * */
    public static Result index() {
        //find current User
        User currentUser = null;
        if (SecureSocial.currentUser() != null) {
            String idCurrent = SecureSocial.currentUser().identityId().userId();
            currentUser = User.find.byId(idCurrent);
        }
        return ok(views.html.main.render(currentUser));
    }

    /*
    * creates an Event by a User in db to publish it
    * */
    //Create Event only possible for logged in Users.. Tag SecuredAction doesn't lock it!
    @SecureSocial.SecuredAction
    @Transactional
    public static Result createEvent() {
        if (SecureSocial.currentUser() != null) {
            Event newEvent = Json.fromJson(request().body().asJson(), Event.class);

            //Save picture name and reset tempName
            if (tempName != null) {
                newEvent.picture = tempName + ".png";
            }
            tempName = null;

            //save false values instead of NULL
            if (newEvent.isVeggie == null) newEvent.isVeggie = false;
            if (newEvent.isAlc == null) newEvent.isAlc = false;
            if (newEvent.isSmoker == null) newEvent.isSmoker = false;
            if (newEvent.isCouple == null) newEvent.isCouple = false;
            if (newEvent.capacity == null) newEvent.capacity = 1;
            if (newEvent.costs == null) newEvent.costs = 0.00;

            //adjust date-String to yyyy-mm-ddThh-mm format
            String str;
            str = newEvent.date.substring(0, 16); //includes date required!
            newEvent.date = str;

            //set capacityLeft as capacity in the beginning
            newEvent.capacityLeft = newEvent.capacity;

            //find current User
            String idCurrent = SecureSocial.currentUser().identityId().userId();
            newEvent.host = User.find.byId(idCurrent);

            final Geocoder geocoder = new Geocoder();
            double latitude = 0.0;
            double longitude = 0.0;

            GeocoderRequest geoReq = new GeocoderRequestBuilder().setAddress(newEvent.location).setLanguage("de").getGeocoderRequest();
            try {
                GeocodeResponse geoResponse = geocoder.geocode(geoReq);
                if (geoResponse.getStatus() == GeocoderStatus.OK) {
                    List<GeocoderResult> resultList = geoResponse.getResults();
                    GeocoderResult result = resultList.get(0);
                    GeocoderGeometry geometry = result.getGeometry();
                    latitude = geometry.getLocation().getLat().doubleValue();
                    longitude = geometry.getLocation().getLng().doubleValue();
                    newEvent.latitude = latitude;
                    newEvent.longitude = longitude;
                }
            } catch (IOException e) {
                newEvent.latitude = 0.0;
                newEvent.longitude = 0.0;
                e.printStackTrace();
            }

            newEvent.save();
            return created(Json.toJson(newEvent));
        }
        return notFound();
    }

    /*
    * uploads a picture for an Event to Server with unique name
    * */
    @Transactional
    public static Result uploadPic() {
        if (SecureSocial.currentUser() != null) {
            Http.RequestBody body = request().body();
            Http.MultipartFormData formData = body.asMultipartFormData();
            Http.MultipartFormData.FilePart file = formData.getFile("file");
            File picture = file.getFile();
            tempName = picture.getName();
            picture.renameTo(new File("public/images/users", tempName + ".png"));
            return ok("File uploaded");
        }
        return notFound();
    }

    /*
    * reads all future Events from db
    * */
    @Transactional
    public static Result getEvents() {
        Date date = new Date();
        String now = df.format(date);
        List<Event> allEvents = Event.find.where().gt("date", now).findList();
        return allEvents.isEmpty() ? notFound() :ok(Json.toJson(allEvents));
    }

    /*
    * reads an Event for a specific id from db
    * */
    @Transactional
    public static Result getEvent(int id) {
        Event event = Event.find.byId(String.valueOf(id));
        return event == null ? notFound() :ok(Json.toJson(event));
    }

    /*
    * reads all Events by User from db
    * */
    //Create Event only possible for logged in Users.. Tag SecuredAction doesn't lock it!
    @SecureSocial.SecuredAction(ajaxCall = true)
    @Transactional
    public static Result getEventByHost() {
        if (SecureSocial.currentUser() != null) {
            String idCurrent = SecureSocial.currentUser().identityId().userId();
            User currentUser = User.find.byId(idCurrent);
            List<Event> allEvents = Event.find.where().eq("host", currentUser).findList();
            return ok(Json.toJson(allEvents));
        }
        return notFound();
    }

    /*
    * updates an existing Event with specific id in db
    * */
    @Transactional
    public static Result updateEvent(int id) {
        if (SecureSocial.currentUser() != null) {
            Event updatedEvent = Json.fromJson(request().body().asJson(), Event.class);
            updatedEvent.id = id;

            //Save picture name and reset tempName
            if (tempName != null) {
                updatedEvent.picture = tempName + ".png";
            }
            tempName = null;

            //adjust date-String to yyyy-mm-ddThh-mm format
            String str;
            str = updatedEvent.date.substring(0, 16); //includes date required!
            updatedEvent.date = str;

            updatedEvent.update();
            return ok(Json.toJson(updatedEvent));
        }
        return notFound();
    }

    /*
    * deletes an existing Event with specific id from db, as well as all dependent Registrations
    * */
    @Transactional
    public static Result deleteEvent(int id) {
        if (SecureSocial.currentUser() != null) {
            Event event = Ebean.find(Event.class, id);

            SqlUpdate deleteRegistrations = Ebean.createSqlUpdate("DELETE FROM Registration WHERE event = :eventId AND id IS NOT NULL");
            deleteRegistrations.setParameter("eventId", id);
            deleteRegistrations.execute();

            Ebean.delete(event);
            return noContent();
        }
        return notFound();
    }

}
